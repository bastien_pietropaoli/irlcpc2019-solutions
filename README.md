# IrlCPC 2019

This contains the problem set used for IrlCPC 2019. Each folder contains the following:

* The statement of the problem (in statement/).
* The inputs used to test the answers (s1 and s2 are the provided samples (which were not tested), b1 and b2 were potential backups in case of issues, input0-9 were the ones used to score points).
* The outputs against which answers were compared (those were generated with other solutions, often brute force, or triple checked with multiple solutions developed separately by multiple people).
* The checker used if any (as in Pandora's question).
* A solution that would score 100% on the problem on the day (specifically tested on the actual submission server).

If you discover any mistake/discrepancy, we'll be happy to discuss them and you can contact us at the following addresses:

* bastien.pietropaoli@insight-centre.org
* milan.decauwer@insight-centre.org

Be aware though that it will not change anything for IrlCPC 2019. The ranks will stay the same, even if a major mistake was to be discovered.



