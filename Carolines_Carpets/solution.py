################################################################################
# Caroline's cheap carpets:
# -------------------------
# Sierpinsky's fractal square.
# Solution provided by: ACM student chapter
################################################################################
import math

def isInsideSierpinskiCarpet(x, y, w, h, nRec):
    # Break the recursive call on reaching rec0.
    # Any point queried on rec0 is indeed part of the carpet.
    if nRec == 0: return True

    # This is where we split the quad into 9 parts
    # Now, the pixel/quad we are processing, what
    # part of the 9 quads does this belong?
    x2, y2 = (x * 3) // w, (y * 3) // h

    # Is the quad we are inside of the center?
    # if yes, this quad must not be rendered
    if x2 == 1 and y2 == 1: return False

    # Not center? Go recursive call in
    # order to split the current cube into even
    # smaller cubes
    x -= (x2 * w) // 3
    y -= (y2 * h) // 3

    return isInsideSierpinskiCarpet(x, y, (w // 3) , (h // 3) , nRec - 1)

if __name__ == "__main__":

    # Parsing params
    dim, nRec, nPts = map(int, input().strip().split(" "))
    lPts = []

    # Parsing queried points
    for _ in range(nPts):
        lPts.append(list(map(int, input().strip().split(' '))))

    # Solving
    for p in lPts:
        sym = "b" if isInsideSierpinskiCarpet(p[0], p[1], 3**dim, 3**dim, nRec) else "w"
        if p != lPts[-1]:
            print(sym, end = " ")
        else:
            print(sym, end = "")
    print()
