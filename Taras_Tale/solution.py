################################################################################
# Tug of war:
# -----------
# Epic gnome battles using a 1D cellular automaton!
# Solution provided by: Bastien Pietropaoli
################################################################################


def update(state, can_grow):
    """
    Updates the provided state with the rule 110.
    Args:
        state (str): The state to update.
        can_grow (bool): If the line can grow or not.
    Return:
        str -- The new state.
    """
    new_state, size = "", len(state)                 # Init values
    for i, gnome in enumerate(state):                # Loop
        if gnome == "1":                             # Is tugging
            if i == 0: new_state += "1"              # Start of the line
            elif i == size-1: new_state += "1"       # End of the line
            else:                                    # Middle of the line
                if state[i-1] == state[i+1] == "1":  # Two tugging neighbours
                    new_state += "0"
                else: new_state += "1"               # 1 or 0 tugging neighbours

        else:                                        # Is not tugging
            if i == 0: new_state += "0"              # Start of the line
            else:                                    # Look at the front gnome
                if state[i-1] == "1":
                    new_state += "1"
                else: new_state += "0"
    
    if state[-1] == "1" and can_grow:                # Grow if you can and it's necessary
        new_state += "1"
    return new_state
    

# ------------------------------------------------------------------------------

if __name__ == "__main__":
    # Read input:
    required_score, max_gnomes = (int(x) for x in input().split())
    left, right = (x for x in input().split())
    left = left[::-1] # To get them tugging the same direction, with the end at the end.
    score = 0

    # Simulate:
    iteration = 0
    score += right.count("1") - left.count("1")
    #print(iteration, ":", left[::-1], right, " -", score)
    while -required_score < score < required_score:
        left = update(left, len(left) + len(right) < max_gnomes)
        right = update(right, len(left) + len(right) < max_gnomes)
        score += right.count("1") - left.count("1")
        iteration += 1

        # Debug:
        #print(iteration, ":", left[::-1], right, " -", score)
        
    # Print the result:
    if score < 0:
        print("left", iteration)
    else:
        print("right", iteration)




