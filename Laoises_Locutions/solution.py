################################################################################
# Solution to the problem: anagrams of palindromes.
# -------------------------------------------------
# Three solutions proposed here (with n being the size of the text):
#   - Solution in O(n) using Counter (two versions)
#   - Solution in O(n) using custom loop (two versions)
# Solution provided by: Bastien Pietropaoli
################################################################################

import time
import string
from collections import Counter, defaultdict

# ------------------------------------------------------------------------------

def anagram_of_palindrome(s):
    """
    Test if the provided string is an anagram of a palindrome.
    Spaces and punctuation don't count.
    Args:
        s (str): The string to test.
    Return:
        int -- 0 if False, 1 if True.
    """
    # Clean the string first:
    to_delete = string.punctuation + string.whitespace
    cleaned = s
    for c in to_delete:
        cleaned = cleaned.replace(c, "")
    cleaned = cleaned.lower()

    # Count:
    count = Counter(cleaned)
    nb_odd = 0
    for letter, count in count.items():
        if count % 2 == 1:
            nb_odd += 1
            if nb_odd > 1: return 0
    return 1

# ------------------------------------------------------------------------------

def anagram_of_palindrome2(s):
    """
    Test if the provided string is an anagram of a palindrome.
    Spaces and punctuation don't count.
    Clean the string in a different way, slightly more efficient!
    Args:
        s (str): The string to test.
    Return:
        int -- 0 if False, 1 if True.
    """
    # Clean the string:
    to_delete = string.punctuation + string.whitespace
    cleaned = s.lower()
    
    # Count:
    count = Counter(cleaned)
    nb_odd = 0
    for letter, count in count.items():
        if count % 2 == 1:
            if letter not in to_delete:
                nb_odd += 1
            if nb_odd > 1: return 0
    return 1

# ------------------------------------------------------------------------------

def anagram_of_palindrome_custom(s):
    """
    Test if the provided string is an anagram of a palindrome.
    Spaces and punctuation don't count.
    Clean efficiently BUT uses a custom counting loop.
    Args:
        s (str): The string to test.
    Return:
        int -- 0 if False, 1 if True.
    """
    # Clean the string:
    to_delete = string.punctuation + string.whitespace
    clean = s.lower()

    count = defaultdict(int)
    for letter in clean:
        count[letter] += 1
    nb_odd = 0
    for letter, count in count.items():
        if count % 2 == 1:
            if letter not in to_delete:
                nb_odd += 1
            if nb_odd > 1: return 0
    return 1

# ------------------------------------------------------------------------------

def anagram_of_palindrome_custom2(s):
    """
    Test if the provided string is an anagram of a palindrome.
    Spaces and punctuation don't count.
    Uses a custom counting loop.
    Args:
        s (str): The string to test.
    Return:
        int -- 0 if False, 1 if True.
    """
    # Clean the string:
    to_delete = string.punctuation + string.whitespace
    clean = s.lower()

    count = defaultdict(int)
    for letter in clean:
        if letter not in to_delete:
            count[letter] += 1
    nb_odd = 0
    for letter, count in count.items():
        if count % 2 == 1:
            nb_odd += 1
            if nb_odd > 1: return 0
    return 1

# ------------------------------------------------------------------------------

def anagram_of_palindrome_slow(s):
    """
    Test if the provided string is an anagram of a palindrome.
    Spaces and punctuation don't count.
    Intentionally suboptimal solution for comparison.
    Args:
        s (str): The string to test.
    Return:
        int -- 0 if False, 1 if True.
    """
    # Clean the string first:
    to_delete = string.punctuation + string.whitespace
    cleaned = s
    for c in to_delete:
        cleaned = cleaned.replace(c, "")
    cleaned = cleaned.lower()
    cleaned = sorted(cleaned)
    
    # Generate a potential palindrome:
    l = []
    switch = True
    for letter in cleaned:
        l.append(letter) if switch else l.insert(0, letter)
        switch = not switch
    candidate = "".join(l)
    return int(candidate == candidate[::-1])

# ------------------------------------------------------------------------------

if __name__ == "__main__":
    """
    # Tuning tests:
    # Test simple cases:
    inputs = [
        "Run, nurses!", 
        "Whatever works for Lucy.", 
        "I love tacos, I love tacos!", 
        "Tacocat forever!",
        "AAAaaaABbbbddDdssss",
        "sofhqgohqhqohg",
        "Not so clever now, are you?",
        "Animosity is no amity.",
        "Election results. Lies! Let's recount!",
        "A decimal point, I am a dot in place."
    ]
    expected = [1, 0, 1, 0, 1, 0, 0, 1, 1, 1]

    for i, case in enumerate(inputs):
        assert anagram_of_palindrome(case) == expected[i]
        assert anagram_of_palindrome2(case) == expected[i]
        assert anagram_of_palindrome_custom(case) == expected[i]
        assert anagram_of_palindrome_custom2(case) == expected[i]
        assert anagram_of_palindrome_slow(case) == expected[i]

    def measure_time(f, nb_iter, *args):
        start = time.process_time()
        for i in range(nb_iter):
            f(*args)
        return time.process_time() - start

    # Measure time:    
    nb_iter = 5000
    test_case = "fohsfihsiofhsfhsdtyu" * 50

    exec1 = measure_time(anagram_of_palindrome, nb_iter, test_case)
    exec2 = measure_time(anagram_of_palindrome2, nb_iter, test_case)
    exec3 = measure_time(anagram_of_palindrome_custom, nb_iter, test_case)
    exec4 = measure_time(anagram_of_palindrome_custom2, nb_iter, test_case)
    exec5 = measure_time(anagram_of_palindrome_slow, nb_iter, test_case)

    print("Time efficient method 1: {0:.6f}s".format(exec1))
    print("Time efficient method 2: {0:.6f}s".format(exec2))
    print("Time custom efficient 1: {0:.6f}s".format(exec3))
    print("Time custom efficient 2: {0:.6f}s".format(exec4))
    print("Time inefficient:        {0:.6f}s".format(exec5))
    """
    results = []

    # Test the exec time with the actual print (since printing is slow):
    #start = time.process_time()
    nb_strings = int(input())
    for i in range(nb_strings):
        results.append(anagram_of_palindrome2(input()))
        #print(results[-1])
    print(*results)
    #exec_time = time.process_time() - start
    #print("Exec time: {0:.6f}s".format(exec_time))
    
    # Test it's the expected result:
    #with open("inputs/expected10", "r") as f:
    #    for i in range(nb_strings):
    #        assert results[i] == int(f.readline())
