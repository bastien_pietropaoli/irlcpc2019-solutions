################################################################################
# Space holiday:
# --------------
# Solution using a linear grid for indexation.
# Wouldn't be able to handle uneven distributions very well.
# Solution provided by: Bastien Pietropaoli
################################################################################

import math

# Set some global variables linked to the problem's def:
minX, maxX = -50000, 50000
minY, maxY = -50000, 50000
minZ, maxZ = -1000, 1000
cube_size  = 2000
nbX = math.ceil((maxX - minX) / cube_size)
nbY = math.ceil((maxY - minY) / cube_size)
nbZ = math.ceil((maxZ - minZ) / cube_size)

# ------------------------------------------------------------------------------

def indexation(points):
    """Indexes the provided points depending on the provided cube size."""
    index = [[[[] for _ in range(nbZ)] for _ in range(nbY)] for _ in range(nbX)]
    for p in points:
        x, y, z = get_cube(p)
        index[x][y][z].append(p)
    return index

# ------------------------------------------------------------------------------

def get_cube(point):
    """Get the cube coordinates in the index."""
    x, y, z = int((point[1]-minX) / cube_size), int((point[2]-minY) / cube_size), int((point[3]-minZ) / cube_size)
    if x == nbX: x -= 1
    if y == nbY: y -= 1 # These take into account points on the edge of the space
    if z == nbZ: z -= 1 
    return (x, y, z)

# ------------------------------------------------------------------------------

def get_knns(point, index, required_k):
    """Get the k-nearest neighbours of the provided point."""
    x1, y1, z1 = point[1], point[2], point[3]
    x, y, z    = get_cube(point)
    knns       = []
    checked    = []
    worst      = float("inf")

    # Start with close neighbours and go farther if necessary:
    how_far = 1
    while len(knns) < required_k or how_far < 3:
        points = []

        for i in range(max(0, x - how_far), min(x + how_far + 1, nbX)):
            for j in range(max(0, y - how_far), min(y + how_far + 1, nbY)):
                for k in range(max(0, z - how_far), min(z + how_far + 1, nbZ)):

                    if (i, j, k) not in checked:
                        checked.append((i, j, k))
                        cornerX, cornerY, cornerZ = i*cube_size - minX, j*cube_size - minY, k*cube_size - minZ

                        # If the cube might contain good candidates:
                        if distance_point_cube(point, (cornerX, cornerY, cornerZ), 
                                                      (cornerX+cube_size, cornerY+cube_size, cornerZ+cube_size)) <= worst:
                            points += index[i][j][k]
                        #points += index[i][j][k]
                            
        # Check distances to candidates:
        for p in points:
            #if p == point: continue
            knns.append((p[0], ((x1-p[1])**2 +
                                (y1-p[2])**2 +
                                (z1-p[3])**2)))

        # Sort again:
        knns  = sorted(knns, key=lambda jfk:jfk[1])
        if knns[0][1] == 0:
            knns = knns[1:required_k+1]
        else:
            knns = knns[:required_k]
        worst = float("inf") if len(knns) < required_k else knns[-1][1]
        how_far += 1
        if how_far > nbX: break

    return knns

# ------------------------------------------------------------------------------

# I know this is ugly, I know, I know.
# BUT it's actually quite efficient and easy to debug.
# And more importantly, it works.
def distance_point_cube(point, min_corner, max_corner):
    """Gets the minimum distance from a point to a cube."""
    x, y, z = point[1], point[2], point[3]
    min_X, min_Y, min_Z = min_corner[0], min_corner[1], min_corner[2]
    max_X, max_Y, max_Z = max_corner[0], max_corner[1], max_corner[2]
    
    if x > max_X:                                                                    # Right          
        if y > max_Y:                                                                #     Top
            if z > max_Z:   return (x - max_X)**2 + (y - max_Y)**2 + (z - max_Z)**2  #         Back
            elif z < min_Z: return (x - max_X)**2 + (y - max_Y)**2 + (z - min_Z)**2  #         Middle
            else:           return (x - max_X)**2 + (y - max_Y)**2                   #         Front
        elif y < min_Y:                                                              #     Left
            if z > max_Z:   return (x - max_X)**2 + (y - min_Y)**2 + (z - max_Z)**2  #         Back
            elif z < min_Z: return (x - max_X)**2 + (y - min_Y)**2 + (z - min_Z)**2  #         Middle
            else:           return (x - max_X)**2 + (y - min_Y)**2                   #         Front
        else:                                                                        #     Middle
            if z > max_Z:   return (x - max_X)**2 + (z - max_Z)**2                   #         Back
            elif z < min_Z: return (x - max_X)**2 + (z - min_Z)**2                   #         Middle
            else:           return (x - max_X)**2                                    #         Front
    elif x < min_X:                                                                  # Left       
        if y > max_Y:                                                                #     And so forth...                 
            if z > max_Z:   return (x - min_X)**2 + (y - max_Y)**2 + (z - max_Z)**2
            elif z < min_Z: return (x - min_X)**2 + (y - max_Y)**2 + (z - min_Z)**2
            else:           return (x - min_X)**2 + (y - max_Y)**2
        elif y < min_Y:
            if z > max_Z:   return (x - min_X)**2 + (y - min_Y)**2 + (z - max_Z)**2
            elif z < min_Z: return (x - min_X)**2 + (y - min_Y)**2 + (z - min_Z)**2
            else:           return (x - min_X)**2 + (y - min_Y)**2
        else:
            if z > max_Z:   return (x - min_X)**2 + (z - max_Z)**2
            elif z < min_Z: return (x - min_X)**2 + (z - min_Z)**2
            else:           return (x - min_X)**2
    else:
        if y > max_Y:
            if z > max_Z:   return (y - max_Y)**2 + (z - max_Z)**2
            elif z < min_Z: return (y - max_Y)**2 + (z - min_Z)**2
            else:           return (y - max_Y)**2
        elif y < min_Y:
            if z > max_Z:   return (y - min_Y)**2 + (z - max_Z)**2
            elif z < min_Z: return (y - min_Y)**2 + (z - min_Z)**2
            else:           return (y - min_Y)**2
        else:
            if z > max_Z:   return (z - max_Z)**2
            elif z < min_Z: return (z - min_Z)**2
            else:           return 0 # Inside the cube, you dumbass.

# ------------------------------------------------------------------------------

if __name__ == "__main__":
    # Read the input and do a quick index by name:
    N, M, k    = (int(x) for x in input().split())
    quick_look = {}
    for _ in range(N):
        line = input().split()
        quick_look[" ".join(line[:-3])] = (float(line[-3]), float(line[-2]), float(line[-1]))
        
    # Spatial indexation of all the points:
    points = [(name, p[0], p[1], p[2]) for name, p in quick_look.items()]
    index  = indexation(points)

    # Queries:
    for _ in range(M):
        target = input()                         # Get the query
        point  = (target, *quick_look[target])   # Retrieve the point
        knns   = get_knns(point, index, k)       # Get the knns
        result = target + ": "                   # Print!
        for n in knns:
            result += n[0] + ", " #+ "(" + str(n[1]**0.5) + ")" + ", "
        print(result[:-2])





