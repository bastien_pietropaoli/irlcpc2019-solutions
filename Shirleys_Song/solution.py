################################################################################
# The Name Game, by Shirley Ellis (1964)
# --------------------------------------
# Solution to the name game.
# Difficulty: easy
# Solution provided by: Bastien Pietropaoli
################################################################################

def generate_verse(name):
    """Generates the verse of the name game for the provided name."""
    result = ""
    
    if name[0] != "B":
        result += name + ", " + name + ", " + "bo-b" + name[1:] + "\n"
    else:
        result += name + ", " + name + ", " + "bo-" + name[1:] + "\n"

    if name[0] != "F":
        result += "bo-na-na fanna, fo-f" + name[1:] + "\n"
    else:
        result += "bo-na-na fanna, fo-" + name[1:] + "\n"
    
    if name[0] != "M":
        result += "fee fi mo-m" + name[1:] + ", " + name + "!"
    else:
        result += "fee fi mo-" + name[1:] + ", " + name + "!"

    return result

# ------------------------------------------------------------------------------

if __name__ == "__main__":
    print(generate_verse(input()))
