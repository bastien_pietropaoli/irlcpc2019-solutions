################################################################################
# Unique email:
# -------------
# Create emails addresses that are clean and unique.
# Solution provided by: Bastien Pietropaoli
################################################################################

import string, time

# ------------------------------------------------------------------------------

def normalise(s):
    """Normalises the name for an email address."""
    normalised = s.lower()                       # Only lowercase letters
    for l in string.punctuation:                 # Get rid of all punctuation
        normalised = normalised.replace(l, "")   # by replacing by nothing.
    return normalised.replace(" ", ".")          # Replace spaces by dots.

# ------------------------------------------------------------------------------

if __name__ == "__main__":
    nb_names = int(input())                          # Read the numbers of names
    names = {}                                       # Keep tracks of counters
    for i in range(nb_names):                        # Do all names
        s = normalise(input())                       # Normalise the name
        if s not in names:                           # New name
            print(s + "@ucc.ie")                     # Print it
            names[s] = 1                             # Keep track of it
        else:                                        # Already existing
            names[s] += 1                            # Increment count
            print(s + str(names[s]) + "@ucc.ie")     # Add the count to the print
