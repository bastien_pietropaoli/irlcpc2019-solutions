################################################################################
# Aoife's ASCII art:
# ------------------
# String manipulation
# Solution provided by the ACM Student chapter
################################################################################

import math

def decorate_breakableLines(lWords):
    # find len of longest word
    maxLen = max(len(w) for w in lWords)

    lineLength = 0
    curLine = []
    extraPad = 0 if maxLen % 2 == 1 else 1
    flLines = "*" + " *" * (math.ceil(maxLen / 2) + extraPad) + " *"
    print(flLines)

    # Going through all the words.
    for w in lWords:
        # Enough space
        if lineLength + len(curLine) + len(w) <= maxLen:
            lineLength += len(w) # Accounting for the space.
            curLine.append(w)
        else:
            #Not enough space
            pad = (maxLen + extraPad) - (lineLength + len(curLine)-1)

            print('* ' + ' '.join(curLine) + ' '* pad + ' *')
            curLine = [w]
            lineLength = len(w)

    # last line
    pad = (maxLen + extraPad) - (lineLength + len(curLine)-1)

    # Close frame
    print('* ' + ' '.join(curLine) + ' ' * pad + ' *')
    print(flLines)

    return

if __name__ == "__main__":

    nChar, nWords = list(map(int,  input().strip().split(' ')))
    lWords = input().strip().split(" ")

    decorate_breakableLines(lWords)

