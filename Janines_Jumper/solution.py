###############################################################################
# Janine's jolly jumper:
# ----------------------
# A graph problem because we needed one I guess.
# 
# The problem is about finding all strongly connected components
# in directed graphs.
# 
# Outpus were double checked against against
# the networkx implementation of Tarjan.
# 
# Solution provided by: ACM student Chapter.
###############################################################################

from sys import setrecursionlimit
setrecursionlimit(20000) 

class CVertex:
    def __init__(self, key):
        self.id = key
        self.connectedTo = {}

    def addNeighbor(self, nbr):
        self.connectedTo[nbr] = True

    def getConnections(self):        
        return self.connectedTo.keys()

    def getId(self):
        return self.id


class CGraph:
    def __init__(self):
        self.vertList = {}        
        self.dTime = 0
        self.nVert = 0

    def addVertex(self,key):
        self.vertList[key] = CVertex(key)

    def getVertex(self,n):
        if n in self.vertList:
            return self.vertList[n]
        else:
            return None

    def __contains__(self,n):
        return n in self.vertList

    def addEdge(self,f,t):
        if f not in self.vertList:
            self.addVertex(f)
            
        if t not in self.vertList:
            self.addVertex(t)
            
        self.vertList[f].addNeighbor(self.vertList[t])
        
    def getVertices(self):
        return self.vertList.keys()
    
    def init_rec_tar(self):
        
        self.nVert = len(self.vertList)        
        
        # Mark all the vertices as not visited
        self.discovered, self.low = [-1] * self.nVert, [-1] * self.nVert
        self.stackMember, self.st = [False] * self.nVert, [] 
   
        # Recording strongly connected components as we go
        self.conComps = []
        
        for i in self.getVertices():
            if self.discovered[i] == -1: 
                # Recursive call on all vertex i (root)
                # DFS Style
                p = self.rec_tar(i)

        # Recursive calls done.
        # Return components.
        return self.conComps
    
    def rec_tar(self, u): 

        # Initialize discovery dTime and low value 
        self.discovered[u] = self.dTime
        self.low[u] = self.dTime 
        self.dTime += 1
        self.stackMember[u] = True
        self.st.append(u)

        # Explore all neighboring vertices 
        for v in self.vertList[u].getConnections(): 
            # If v is not visited yet, then recur for it 
            if self.discovered[v.id] == -1 : 
            
                self.rec_tar(v.id)
                # Check if the subtree rooted with v has a connection to 
                # one of the ancestors of u
                self.low[u] = min(self.low[u], self.low[v.id]) 
                        
            elif self.stackMember[v.id] == True:  

                #Update low value of 'u' only if 'v' is still in stack                 
                self.low[u] = min(self.low[u], self.discovered[v.id]) 

        
        tempV = -1 #To store stack extracted vertices
        tempConComp = []
        if self.low[u] == self.discovered[u]: 
            while tempV != u: # While not back to head
                tempV = self.st.pop()
                tempConComp.append(tempV)
                self.stackMember[tempV] = False

        if len(tempConComp) != 0:
            self.conComps.append(tempConComp)
        
    
def sortedPrint(conComps):
    # The components have to be given in a certain order.
    comps = {min(conComps[i]):i for i in range(len(conComps))}
    for key in sorted(comps.keys()):        
        print(" ".join(map(str, sorted(conComps[comps[key]]))))

if __name__ == "__main__":
    # We use a custom graph class
    # Implemented above
    CG = CGraph() # Empty Digraph
    
    # Parse input and build graph as we go.
    n = int(input().strip())
    
    for _ in range(n):
        line = list(map(int, input().strip().split(" ")))
        CG.addVertex(line[0])
        for target in line[1:]:
            CG.addEdge(line[0], target)
            
    # Solve using an Implementation of Tarjan's algorithm.
    solTAR = CG.init_rec_tar()
    
    # fancy print solution
    sortedPrint(solTAR)    
