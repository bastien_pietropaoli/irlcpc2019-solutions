################################################################################
# Polygons & Monte Carlo:
# -----------------------
# We'll use the ray tracing technique to determine if a point is within a polygon
# or not. The Monte Carlo method is fairly straightforward.
# Solution provided by: Bastien Pietropaoli
################################################################################

import time, random

# ------------------------------------------------------------------------------

# This could probably be a little bit faster but I can't be bothered to optimise
# it, honestly.
# The point_in method will fail for cases where the ray crosses an intersection
# BUT the Monte Carlo approach makes this negligible. So I don't care.

def point_in(polygon, point):
    """
    Checks wether or not a point is inside a polygon, using raytracing.
    Args:
        polygon (list(list())) - A list of tuples representing the polygon.
        point (tuple) - A tuple representing the point to test.
    Return:
        Bool -- True if the provided point is inside the provided polygon, False
        otherwise.
    """
    # Some info on the polygon and point:
    x, y = point[0], point[1]
    nb_intersections = 0
    n = len(polygon)
    
    # Loop over all of the edges (the last vertex being connected to the first one):
    for i in range(n):
        a_x, a_y = polygon[i][0], polygon[i][1]
        b_x, b_y = polygon[(i+1) % n][0], polygon[(i+1) % n][1]

        # The ray may have a chance to intersect the edge:
        if min(a_y, b_y) <= y <= max(a_y, b_y):

            # Normal slope:
            if b_x != a_x:
                slope     = (b_y - a_y) / (b_x - a_x)
                intercept = a_y - slope * a_x
                if x <= (y - intercept) / slope:
                    nb_intersections += 1

                # If on the segment itself:
                if x * slope + intercept == y:
                    return True

            # Vertical slope:
            else:
                if x <= a_x: nb_intersections += 1

    # An odd number of intersections means you're in the polygon:
    return nb_intersections % 2 == 1

# ------------------------------------------------------------------------------

def surface(polygon, nb_iter=50000000):
    """
    Computes the surface of the provided polygon using a Monte Carlo method.
    Args:
        polygon (list(list())) - A list of tuples representing the polygon.
        nb_iter (int, default=10000) - The number of iterations for the Monte
            Carlo method.
    Return:
        float -- The surface area of the polygon.
    """
    # Get the tightest rectangle:
    xs = [p[0] for p in polygon]
    ys = [p[1] for p in polygon]

    min_x, max_x = min(xs), max(xs)
    min_y, max_y = min(ys), max(ys)
    
    range_x = max_x - min_x
    range_y = max_y - min_y

    # Generate random points and check wether or not they're inside:
    nb_in  = 0
    for i in range(nb_iter):
        x = random.random() * range_x + min_x
        y = random.random() * range_y + min_y
        if point_in(polygon, (x, y)):
            nb_in += 1
    
    print("Nb iterations: ", nb_iter)
    # Do the ratio:
    return range_x * range_y * nb_in / nb_iter

# ------------------------------------------------------------------------------

def surface_max_time(polygon, max_time=1):
    """
    Computes the surface of the provided polygon using a Monte Carlo method
    with a fixed max_time (instead of a fixed number of iterations).
    Args:
        polygon (list(list())) - A list of tuples representing the polygon.
        nb_iter (float, default=1) - The max time in seconds.
    Return:
        float -- The surface area of the polygon.
    """
    # Get the tightest rectangle:
    xs = [p[0] for p in polygon]
    ys = [p[1] for p in polygon]

    min_x, max_x = min(xs), max(xs)
    min_y, max_y = min(ys), max(ys)
    
    range_x = max_x - min_x
    range_y = max_y - min_y

    start = time.time()
    nb_iterations = 0
    nb_in = 0
    while time.time() - start < max_time:
        for i in range(1000):
            x = random.random() * range_x + min_x
            y = random.random() * range_y + min_y
            if point_in(polygon, (x, y)):
                nb_in += 1
        nb_iterations += 1000
    
    #print("Nb iterations: ", nb_iterations)
    return range_x * range_y * nb_in / nb_iterations

# ------------------------------------------------------------------------------

if __name__ == "__main__":
    # If you use a horrible way of passing arguments, I use a horrible method
    # to parse those:
    polygon = []
    for i in range(int(input())):
        polygon.append([float(x) for x in input().split()])

    print(surface_max_time(polygon, max_time=0.85))

    """
    start = time.time()
    result = surface(polygon)
    exec_time = time.time() - start
    print("Exec time: ", exec_time)
    print("Surface of the polygon: ", result)
    print("- " * 20)
    start = time.time()
    result = surface_max_time(polygon, 0.95)
    exec_time = time.time() - start
    print("Exec time: ", exec_time)
    print("Surface of the polygon: ", result)
    """
