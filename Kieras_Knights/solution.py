################################################################################
# Kiera's kooky knights:
# ----------------------
# Solving a typical problem in higher dimension.
# Solution provided by: Bastien Pietropaoli
################################################################################

def how_many_moves(p):
    """Get how many moves you need to go to a certain position."""
    turns = 0
    dist = sum(p)

    while dist > 0:
        # Close cases (figured manually):
        if dist == 1: return turns + 3
        if dist == 2: return turns + 2
        if dist == 3:
            if p.count(1) == 3: return turns + 3
            else:               return turns + 1
        if dist == 4:
            if p.count(1) == 4: return turns + 4
            else:               return turns + 2
        
        # Obvious move:
        if 2 in p and 1 in p:
            p[p.index(2)] = 0
            p[p.index(1)] = 0

        # Get closer (remember, here, distance is at least 5):
        else:
            sorted_p = sorted(p)
            p[p.index(sorted_p[-1])] -= 2  # Largest gets -2
            p[p.index(sorted_p[-2])] -= 1  # Second largest gets -1
        
        # Update:
        turns += 1
        dist = sum(p)

    return turns

# ------------------------------------------------------------------------------
        
if __name__ == "__main__":
    # Read the input:
    input() # Useless
    p1 = [int(x) for x in input().split()]
    p2 = [int(x) for x in input().split()]

    # Don't care about the actual positions, just care about the difference:
    print(how_many_moves([abs(x - y) for x, y in zip(p1, p2)]))
